﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightManager : MonoBehaviour
{


    public List<GameObject> Lights;
    public GameObject skyObject;
    public TOD_Sky sky;
    public bool IsSwitch;

    // Use this for initialization
    void Start()
    {
        sky = skyObject.GetComponent<TOD_Sky>();

    }

    public void TurnOn()
    {
        foreach (var item in Lights)
        {
            item.SetActive(true);
        }
    }

    public void TurnOff()
    {
        foreach (var item in Lights)
        {
            item.SetActive(false);
        }

    }
    // Update is called once per frame
    void Update()
    {

        if (sky.Cycle.Hour >= 18.2 && sky.Cycle.Hour <= 18.205)
        {

            int x = 0;

            //  TurnOn();
            foreach (var item in Lights)
            {
                item.SetActive(true);
                x++;

                Debug.Log(x);

            }



        }

    }
    //IEnumerator LightsOn()
    //{

    //}


}