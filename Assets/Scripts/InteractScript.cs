﻿using UnityEngine;
using System.Collections;

public class InteractScript : MonoBehaviour {

    public float interactDistance = 5f;
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, interactDistance ))
            {
                if(hit.collider.CompareTag("Door"))
                {
                    hit.collider.transform.parent.GetComponent<DoorScript>().doorState();
                }

                if(hit.collider.CompareTag("Window_1"))
                {
                    hit.collider.GetComponent<Window_1_Open>().WindowState();
                }
            }

        }
	}
}
