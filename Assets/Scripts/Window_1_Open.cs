﻿using UnityEngine;
using System.Collections;

public class Window_1_Open : MonoBehaviour {

    public float interactDistance = 5f;
 
    public bool isOpen = false;
    
    public void WindowState()
    {
        if (isOpen == false)
        {
            GetComponent<Animation>().Play("window_1_open");
            isOpen = true;

        }
        else if (isOpen == true)
        {
            GetComponent<Animation>().Play("window_1_close");
            isOpen = false;
        }

    }

  
    
  
}
