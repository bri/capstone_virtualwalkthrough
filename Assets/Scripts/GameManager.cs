﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public delegate void WallsDown();
    public static event WallsDown OnWallsDown;

    public delegate void WallsUp();
    public static event WallsUp OnWallsUp;

    private bool isWallsDown;

  public GameObject sky;
    public GameObject Cam;
    private FreeCameraMove freeCam;
   private FirstPersonController firstCam;

    void Awake()
    {
        if (instance == null)
            instance = this;
     
    }
	// Use this for initialization
	void Start () {

        freeCam = Cam.GetComponent<FreeCameraMove>();
        firstCam = Cam.GetComponent<FirstPersonController>();
        isWallsDown = false;

		if (sky != null)
		{
			sky.SetActive(true);
		}
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.F))
        {

            var rot = Cam.transform.rotation.eulerAngles;
            Debug.Log(rot);
            if(freeCam.enabled)
            {
              
                freeCam.enabled = false;
            
                firstCam.enabled = true;
                Cam.transform.rotation = Quaternion.Euler(rot);
                Debug.Log(Cam.transform.rotation.eulerAngles + "after inner");
                return;
            }
            freeCam.enabled=true;
         
            firstCam.enabled=false;
            Cam.transform.rotation = Quaternion.Euler(rot);
            Debug.Log(Cam.transform.rotation.eulerAngles + "after outer");


        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (isWallsDown)
            {

                if (OnWallsUp!= null)
                    OnWallsUp();


                isWallsDown = false;

              
            }
            else
            {

                if (OnWallsDown != null)
                    OnWallsDown();
                isWallsDown = true;
               
            }

        }

	
	}
}
