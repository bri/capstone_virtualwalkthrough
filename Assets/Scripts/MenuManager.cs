﻿using UnityEngine;
using System.Collections;
using MaterialUI;
using UnityStandardAssets.Characters.FirstPerson;

public class MenuManager : MonoBehaviour {

    public GameObject menuPanel;
    public GameObject helpPanel;
    public GameObject timePanel;
    public GameObject switchPanel;

   

    //TODO: same sound clip per panel????
    //public AudioClip MenuSoundA;
    //public AudioClip MenuPanelSoundB;

    public AudioClip TimePanelSoundA;
    public AudioClip TimePanelsoundB;
    

    private FirstPersonController firstCam;

    public GameObject fpc;

    float delayTime = .3f;
    float timePassed = 0;
    bool isCursorLocked = false;

    AudioSource audioSource;

    Animator menuPanelAnim;
    void Start()
    {
        menuPanelAnim = menuPanel.GetComponent<Animator>();
        firstCam = fpc.GetComponent<FirstPersonController>();
        gameObject.AddComponent<AudioSource>();

        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;

    }

    public void ShowCursor()
    {
        firstCam.m_MouseLook.XSensitivity = 0;
        firstCam.m_MouseLook.YSensitivity = 0;
        firstCam.m_WalkSpeed = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    IEnumerator CloseEffect()
    {



        //cameraMaster.transform.rotation = Quaternion.Lerp
        //(cameraMaster.transform.localRotation, cameraList
        //[index].transform.localRotation, easeSpeed);
        //  yield return new WaitForSeconds(1);
        //Debug.Log("name hash" + menuPanelAnim.GetCurrentAnimatorStateInfo(0).fullPathHash);
        Debug.Log("animator state" + menuPanelAnim.GetCurrentAnimatorStateInfo(0).ToString());
        while (menuPanelAnim.GetCurrentAnimatorStateInfo(0).IsName("MenuPanel_Open")) 
        {
            yield return null;
        }
      
        menuPanel.SetActive(false);

    }
    // Update is called once per frame1
    void Update()
    {
      
        timePassed += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Alpha1) && timePassed >= delayTime)
        {
       
            if (menuPanel.activeSelf)
            {
                audioSource.PlayOneShot(TimePanelsoundB);
                menuPanelAnim.SetTrigger("Close");
                StartCoroutine(CloseEffect());
               
                HideCursor();
            }

            else
            {
            
                audioSource.PlayOneShot(TimePanelSoundA);
                menuPanel.SetActive(true);
                menuPanelAnim.SetTrigger("Open");

                ShowCursor();
            }
            
            timePassed = 0;
          
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (timePanel.activeSelf)
            {
                //    audioSource.clip = TimePanelSoundA;
                audioSource.PlayOneShot(TimePanelsoundB);
                timePanel.SetActive(false);
                HideCursor();
                firstCam.m_MouseLook.XSensitivity = 2f;
                firstCam.m_MouseLook.YSensitivity = 2f;
            }

            else
            {
                audioSource.PlayOneShot(TimePanelSoundA);
                timePanel.SetActive(true);
                ShowCursor();
                firstCam.m_MouseLook.XSensitivity = 0;
                firstCam.m_MouseLook.YSensitivity = 0;
                //DialogManager.ShowTimePicker(Random.Range(0, 12), Random.Range(0, 60), Random.value > 0.5f, (int hour, int minute, bool isAM) =>
                //{
                //    ToastManager.Show(hour + ":" + minute.ToString("00") + " " + (isAM ? "AM" : "PM"));
                //}, MaterialColor.Random500());
            }


        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (switchPanel.activeSelf)
            {
                //    audioSource.clip = TimePanelSoundA;
                audioSource.PlayOneShot(TimePanelsoundB);
                switchPanel.SetActive(false);
                HideCursor();
                firstCam.m_MouseLook.XSensitivity = 2f;
                firstCam.m_MouseLook.YSensitivity = 2f;
            }

            else
            {
                audioSource.PlayOneShot(TimePanelSoundA);
                switchPanel.SetActive(true);
                ShowCursor();
                firstCam.m_MouseLook.XSensitivity = 0;
                firstCam.m_MouseLook.YSensitivity = 0;
                //DialogManager.ShowTimePicker(Random.Range(0, 12), Random.Range(0, 60), Random.value > 0.5f, (int hour, int minute, bool isAM) =>
                //{
                //    ToastManager.Show(hour + ":" + minute.ToString("00") + " " + (isAM ? "AM" : "PM"));
                //}, MaterialColor.Random500());
            }



            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                helpPanel.SetActive(true);
            }
        }
    }

    public void HideCursor()
    {
        firstCam.m_MouseLook.XSensitivity = 2;
        firstCam.m_MouseLook.YSensitivity = 2;
        firstCam.m_WalkSpeed = 5;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
