﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

    public bool open = false;
    public float openDoorAngle = 90f;
    public float closeDoorAngle = 0f;
    public float smooth = 2f;
	// Use this for initialization
	void Start () {
	
	}
	
    public void doorState()
    {
        open = !open;

    }
	// Update is called once per frame
	void Update () {

        if(open)
        {
            Quaternion rotateTarget = Quaternion.Euler(0, openDoorAngle, 0);          
            transform.localRotation = Quaternion.Slerp(transform.localRotation, rotateTarget, smooth * Time.deltaTime);
        }
        else
        {
            Quaternion rotateTarget2 = Quaternion.Euler(0, closeDoorAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, rotateTarget2, smooth * Time.deltaTime);

        }
	
	}
}
