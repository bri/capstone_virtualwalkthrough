﻿using UnityEngine;
using System.Collections;

public class WallsDown : MonoBehaviour {


    MeshRenderer meshRenderer;
	// Use this for initialization
	void Start () {

        meshRenderer = GetComponent<MeshRenderer>();
        col = GetComponent<MeshCollider>();

        GameManager.OnWallsDown += MakeWallsDown;
        GameManager.OnWallsUp += MakeWallsUp;

	}


    void OnDisable()
    {
        GameManager.OnWallsDown -= MakeWallsDown;
        GameManager.OnWallsUp -= MakeWallsUp;

    }

    void MakeWallsDown()
    {
        if (meshRenderer.enabled == true)
            meshRenderer.enabled = false;


        if (col != null)
            col.enabled = false;
      
    }

    void MakeWallsUp()
    {
        if (meshRenderer.enabled == false)
            meshRenderer.enabled = true;


        if (col != null)
            col.enabled = true;
    }

    MeshCollider col;
}
