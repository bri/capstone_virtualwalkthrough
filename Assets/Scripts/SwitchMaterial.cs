﻿using UnityEngine;
using System.Collections;

public class SwitchMaterial : MonoBehaviour {

    public GameObject Ground_DiningHall_Floor;
    public Material Ground_Texture1;
    public Material Ground_Texture2;

    Renderer rGround_DiningHall_Floor;
    public void SwitchTexture1()
    {
        rGround_DiningHall_Floor.material = Ground_Texture1;

    }

    public void SwitchTexture2()
    {
        rGround_DiningHall_Floor.material = Ground_Texture2;

    }

    // Use this for initialization
    void Start () {
        rGround_DiningHall_Floor = Ground_DiningHall_Floor.GetComponent<Renderer>();

    }

    // Update is called once per frame
    void Update () {
	
	}
}
