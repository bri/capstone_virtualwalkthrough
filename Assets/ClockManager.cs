﻿using UnityEngine;
using System.Collections;
using MaterialUI;
using UnityEngine.EventSystems;

public class ClockManager : MonoBehaviour {

	// Use this for initialization
	 DialogTimePicker timePicker;
	TOD_Sky sky;

	public GameObject Sky;
	public GameObject Time;




    void Start () {

		sky = Sky.GetComponent<TOD_Sky>();
		timePicker = Time.GetComponent<DialogTimePicker>();
	  
	}


	public void IsClick()
	{
      
		if (sky.Cycle.Hour >= 12)
			timePicker.isAM = false;
		else
			timePicker.isAM = true;

		timePicker.SetTime((int)(sky.Cycle.Hour), 0);
	}
	void SetDate()
	{
        Debug.Log(timePicker.currentHour);

		if (timePicker.isAM)
		{
			sky.Cycle.Hour = timePicker.currentHour;

		}
		else
		{
			sky.Cycle.Hour = timePicker.currentHour + 12;
		}
	}

	

	// Update is called once per frame
	void Update () {


		if (Time.activeSelf)
		{
        
            Debug.Log(Input.GetMouseButton(1));
        
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
             
                SetDate();
            }
		  
		}

		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			IsClick();
		}
	  
	}
}
