﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{

    public GameObject[] cameraList;
    //public Camera cameraMaster;
    public GameObject cameraMaster;
    public float easeSpeed = 0.125f;
    CharacterController cMasterCharCntlr;
    

    private int index = 0;

    void Start()
    {
        cMasterCharCntlr = cameraMaster.GetComponent<CharacterController>();
        foreach (GameObject cam in cameraList)
        {
            cam.SetActive(false);

        }
        //cameraList[index].SetActive(true);

        //foreach (Camera cam in cameraList)
        //{
        //    cam.enabled = false;
        //}
        //cameraList[index].enabled = true;
        var test = cameraMaster.GetComponentInChildren<Camera>();
        Debug.Log("camera master position: " + test.transform.position);

    }
    public void SwitchCam()
    {
        //#region
        //cameraList[index].SetActive(false);

        //index = (index + 1) % cameraList.Length;
        //cameraList[index].SetActive(true);
        //#endregion


        index = (index + 1) % cameraList.Length;
        Debug.Log(cameraList[index].transform.position);
        //    #region for smooth transition
        //    while (Mathf.Ceil(cameraMaster.transform.position.x) != Mathf.Ceil(cameraList[index].transform.position.x))
        //    {

        //        cameraMaster.transform.position = Vector3.Lerp
        //(cameraMaster.transform.position, cameraList
        //[index].transform.position, easeSpeed);
        //        cameraMaster.transform.rotation = Quaternion.Lerp
        //        (cameraMaster.transform.localRotation, cameraList
        //        [index].transform.localRotation, easeSpeed);
        //    }
        //    #endregion
        cMasterCharCntlr.enabled = false; 
        StartCoroutine(Smooth());
    }

    IEnumerator Smooth()
    {
        while (Mathf.Ceil(cameraMaster.transform.position.x) != Mathf.Ceil(cameraList[index].transform.position.x))
        {

            cameraMaster.transform.position = Vector3.Lerp
    (cameraMaster.transform.position, cameraList
    [index].transform.position, easeSpeed);

            //cameraMaster.transform.rotation = Quaternion.Lerp
            //(cameraMaster.transform.localRotation, cameraList
            //[index].transform.localRotation, easeSpeed);
            yield return null;
        }
        Debug.Log("destination reached");
       
        print("smooth cRoutine finished");
        cMasterCharCntlr.enabled = true;
    }
    // Update is called once per frame
    void Update()
    {






    }
}
